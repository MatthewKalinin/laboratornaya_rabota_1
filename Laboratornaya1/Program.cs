﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Laboratornaya1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*для проверок
            Record r1 = new Record() { firstName="qwerty1", secondName="qwer1", thirdName = "qwe1", phone = 9123456789, country="russia",dateBirth = new DateTime(2001,8,29), job="", organization="", other="я первый пользователь"};
            Record r2 = new Record() { firstName="qwerty2", secondName="qwer2", thirdName = "qwe2", phone = 9123445289, country="ukraine",dateBirth = new DateTime(2001,9,29), job="", organization="", other=""};
            Record r3 = new Record() { firstName="qwerty3", secondName="qwer3", thirdName = "qwe3", phone = 9122345289, country="russia",dateBirth = new DateTime(2020,8,29), job="sdfs", organization="sdfdsf", other="я первый пользователь"};
            Record r4 = new Record() { firstName="qwerty4", secondName="qwer4", thirdName = "qwe4", phone = 9123346789, country="penza",dateBirth = new DateTime(2001,9,15), job="", organization="", other="я первый пользователь"};
            Record r5 = new Record() { firstName="qwerty5", secondName="qwer5", thirdName = "qwe5", phone = 9123456789, country="russia",dateBirth = new DateTime(2001,8,29), job="sdfd", organization="4erwwer", other=""};
            PhoneBook.listRecord.Add(r1);
            PhoneBook.listRecord.Add(r2);
            PhoneBook.listRecord.Add(r3);
            PhoneBook.listRecord.Add(r4);
            PhoneBook.listRecord.Add(r5);*/

            while (true)
            {
                if (!Interface.DefoltInterface())
                {
                    break;
                }
            }
        }
    }

    public static class Interface
    {
        public static bool DefoltInterface()
        {
            Console.Clear();
            Console.WriteLine("Выберите действие с помощью клавиш 1 - 5");
            Console.WriteLine("1 - Добавить запись");
            Console.WriteLine("2 - Удалить запись");
            Console.WriteLine("3 - Редактировать запись");
            Console.WriteLine("4 - Показать все записи");
            Console.WriteLine("5 - Показать одну запись");
            Console.WriteLine("0 или иной символ - ВЫХОД");
            ConsoleKeyInfo key = Console.ReadKey();
            switch (key.KeyChar)
            {
                case '1': //Создание карточки
                    Console.Clear();
                    PhoneBook.CreateRecord();
                    Console.WriteLine("Запись успешно создана!");
                    Thread.Sleep(1000);
                    break;
                case '2': //Удаление карточки
                    Console.Clear();
                    if (PhoneBook.listRecord.Count == 0)
                    {
                        Console.WriteLine("Не найдено ни одной записи");
                        Thread.Sleep(1000);
                        break;
                    }
                    PhoneBook.GetAllRecord();
                    Console.Write("Введите номер записи, которую хотите удалить: ");
                    int id;
                    while (!int.TryParse(Console.ReadLine(),out id))
                    {
                        Console.Write("Некорректно введен номер записи, попробуйте снова: ");
                    }
                    if (!PhoneBook.DeleteRecord(id))
                    {
                        Console.WriteLine("Нет записи с выбранным номером");
                    }
                    break;
                case '3': //Редактирование карточки
                    Console.Clear();
                    if (PhoneBook.listRecord.Count == 0)
                    {
                        Console.WriteLine("Не найдено ни одной записи");
                        Thread.Sleep(2000);
                        break;
                    }
                    PhoneBook.GetAllRecord();
                    Console.Write("Введите номер записи, которую хотите удалить: ");
                    while (!int.TryParse(Console.ReadLine(), out id))
                    {
                        Console.Write("Некорректно введен номер записи, попробуйте снова: ");
                    }
                    if (!PhoneBook.EditRecord(id))
                    {
                        Console.WriteLine("Нет записи с выбранным номером");
                    }
                    break;
                case '4'://показать всё
                    Console.Clear();
                    PhoneBook.GetAllRecord();
                    Console.WriteLine("Нажмите люмую клавишу, чтобы выйти");
                    Console.ReadKey();
                    break;
                case '5'://показать только выбранную
                    Console.Clear();
                    if (PhoneBook.listRecord.Count == 0)
                    {
                        Console.WriteLine("Не найдено ни одной записи");
                        Thread.Sleep(2000);
                        break;
                    }
                    PhoneBook.GetAllRecord();
                    Console.Write("Введите номер записи, которую хотите удалить: ");
                    while (!int.TryParse(Console.ReadLine(), out id))
                    {
                        Console.Write("Некорректно введен номер записи, попробуйте снова: ");
                    }
                    if (!PhoneBook.ShowRecord(id))
                    {
                        Console.WriteLine("Нет записи с выбранным номером");
                    }
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    static class PhoneBook
    {
        public static List<Record> listRecord = new List<Record>();

        public static void CreateRecord()
        {
            Record record = new Record();
            //ФИО(обязательные поля)
            record.EditName();
            //Телефон(обязательное поле)
            record.EditPhoneNum();
            //Страна(обязательное)
            record.EditCountry();
            //дата рождения
            record.EditBirthDate();
            //организация
            record.EditOrganization();
            //должность
            record.EditJob();
            //прочие заметки
            record.EditOther();

            listRecord.Add(record);
        }
        public static void GetAllRecord()
        {
            for (int i = 0; i < listRecord.Count; i++)
            {
                Console.WriteLine(i + 1 + " " + listRecord[i].secondName + " " + listRecord[i].firstName + " " + listRecord[i].phone);
            }
        }
        public static bool DeleteRecord(int id)
        {
            if (id > 0 && id <= listRecord.Count)
            {
                listRecord.RemoveAt(id - 1);
                Console.WriteLine("Запись успешно удалена!");
                return true;
            }
            else
                return false;
        }
        public static bool EditRecord(int id)
        {
            id--;

            if (id < 0 || id >= listRecord.Count)
            {
                return false;
            }

            Console.WriteLine("1 - редактировать ФИО");
            Console.WriteLine("2 - редактировать номер телефона");
            Console.WriteLine("3 - изменить страну");
            Console.WriteLine("4 - изменить дату рожения");
            Console.WriteLine("5 - изенить организацию");
            Console.WriteLine("6 - изенить должность");
            Console.WriteLine("7 - изенить заметрки");

            ConsoleKeyInfo key = Console.ReadKey();
            Console.Clear();
            switch (key.KeyChar)
            {
                case '1'://Изменение ФИО
                    listRecord[id].EditName();
                    Console.WriteLine("ФИО успешно обновлено");
                    break;
                case '2'://изменение номера
                    listRecord[id].EditPhoneNum();
                    Console.WriteLine("Номер успешно изменен");
                    break;
                case '3'://изменение страны
                    listRecord[id].EditCountry();
                    Console.WriteLine("Страна успешна изменена");
                    break;
                case '4'://изменение даты рождения
                    listRecord[id].EditBirthDate();
                    Console.WriteLine("Дата рождения успешна изменена");
                    break;
                case '5'://изменение организации
                    listRecord[id].EditOrganization();
                    Console.WriteLine("Организация успешна изменена");
                    break;
                case '6'://изменение должности
                    listRecord[id].EditJob();
                    Console.WriteLine("Должность успешна изменена");
                    break;
                case '7'://изменение заметок
                    listRecord[id].EditOther();
                    Console.WriteLine("Заметки успешно изменены");
                    break;
                default:
                    Console.WriteLine("Введено некорректное значение");
                    break;
            }
            return true;
        }
        public static bool ShowRecord(int id)
        {
            id--;
            if (id < 0 || id >= listRecord.Count)
            {
                return false;
            }
            Record record = listRecord[id];
            Console.WriteLine("Фамилия: " + record.secondName);
            Console.WriteLine("Имя: " + record.firstName);
            if (record.thirdName != "")
                Console.WriteLine("Отчество: " + record.thirdName);

            Console.WriteLine("Номер телефона: +7" + record.phone);
            Console.WriteLine("Страна: " + record.country);

            if (record.dateBirth != DateTime.MinValue)
                Console.WriteLine("Дата рождения: " + record.dateBirth.ToShortDateString());
            if (record.organization != "")
                Console.WriteLine("Организация: " + record.organization);
            if (record.job != "")
                Console.WriteLine("Должность: " + record.job);
            if (record.other != "")
                Console.WriteLine("Заметки: " + record.other);

            Console.WriteLine("Нажмите любую клавишу, чтобы выйти");
            Console.ReadKey();
            return true;
        }
    }

    class Record
    {
        public string firstName, secondName, thirdName;
        public long phone;
        public string country;
        public DateTime dateBirth;
        public string organization;
        public string job;
        public string other;

        public void EditName()
        {
            Console.Write("Имя: ");
            string firstName = Console.ReadLine();
            Console.Write("Фамилия: ");
            string secondName = Console.ReadLine();
            Console.Write("Отчество: ");
            string thirdName = Console.ReadLine();
            while (firstName == "" || secondName == "")
            {
                Console.WriteLine("Вы не ввели одно из полей, повторите попытку");
                Console.Write("Имя: ");
                firstName = Console.ReadLine();
                Console.Write("Фамилия: ");
                secondName = Console.ReadLine();
                Console.Write("(необязательно) Отчество: ");
                thirdName = Console.ReadLine();
            }
            this.firstName = firstName;
            this.secondName = secondName;
            this.thirdName = thirdName;
        }
        public void EditPhoneNum()
        {
            long phone;
            Console.Write("Номер телефона (только цифры): ");
            while (!long.TryParse(Console.ReadLine(), out phone))
            {
                Console.WriteLine("Неверно введен номер телефона, попробуйте снова:");
            }
            this.phone = phone;
        }
        public void EditCountry()
        {
            Console.Write("Страна: ");
            string country = Console.ReadLine();
            while (country == "")
            {
                Console.WriteLine("Вы не ввели страну проживания");
                country = Console.ReadLine();
            }
            this.country = country;
        }
        public void EditBirthDate()
        {
            Console.Write("День рождения: ");
            int.TryParse(Console.ReadLine(), out int day);
            Console.Write("Месяц рождения (без 0 вначале): ");
            int.TryParse(Console.ReadLine(), out int mounth);
            Console.Write("Год рождения: ");
            int.TryParse(Console.ReadLine(), out int year);

            if (day == 0 || mounth == 0 || year == 0)
                this.dateBirth = DateTime.MinValue;
            else
                this.dateBirth = new DateTime(year, mounth, day);
        }
        public void EditOrganization()
        {
            Console.Write("Организация: ");
            this.organization = Console.ReadLine();
        }
        public void EditJob()
        {
            Console.Write("Должность: ");
            this.job = Console.ReadLine();
        }
        public void EditOther()
        {
            Console.WriteLine("Дополнительные заметки: ");
            this.other = Console.ReadLine();
        }
    }
}
